from django.shortcuts import render
from .models import News

from django.conf.urls import patterns, url
from news import views
urlpatterns = patterns('',
 url(r'^$', views.index, name='index'),
)



def index(request):
 news_list = News.objects.all()
 context = {'news': news_list}
 return render(request, 'news/index.html', context)
