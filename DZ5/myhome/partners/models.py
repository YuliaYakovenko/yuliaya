from django.db import models

class Partners(models.Model):
	title = models.CharField(max_length=70)
	description = models.CharField(max_length=255)
	logo = models.ImageField(upload_to='pic/...png')
	text = models.TextField()