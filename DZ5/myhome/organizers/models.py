from django.db import models

class Organizers(models.Model):
	title = models.CharField(max_length=70)
	description = models.CharField(max_length=255)
	text = models.TextField()