from django.forms import ModelForm
from myapp.models import Newws

# Create the form class.
	class NewwsForm(ModelForm):
		class Meta:
			model = Newws
			fields = ['pub_date', 'headline', 'content', 'reporter']

# Creating a form to add an article.
 form = NewwsForm()

# Creating a form to change an existing article.
article = Newws.objects.get(pk=1)
form = NewwsForm(instance=article)