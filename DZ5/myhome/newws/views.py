from django.shortcuts import render
from .models import Newws
def index(request):
	newws_list = Newws.objects.all()
	context = {'newws': newws_list}
	return render(request, 'newws/index.html', context)
